#!/bin/bash

# bzip2 --keep -# INPUT_FILE
#
# --keep
# 	Don't delete input_file
#
# -#
# 	Compression level, '#' is a digit ch(1 to 9)
#

COMP_LEVEL=$1		# Compression level
SRC=$2			# Input_file


bzip2 --keep -${COMP_LEVEL} ${SRC}
