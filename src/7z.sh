#!/bin/bash

# 7z a -mx# OUTPUT_FILE INPUT_FILE
#
# a
# 	Add to archive
#
# -m#
# 	Comperssion level, '#' is a digit ch(1 to 9)
#


COMP_LEVEL=$1		# Comperssion level
SRC=$2			# Input file


7z a -mx${COMP_LEVEL} ${SRC}.7z ${SRC}
