#!/bin/bash

# xz --keep -# INPUT_FILE
#
# --keep
# 	Don't delete input file
#
# -#
# 	Compression level, '#' is a digit ch(1 to 9)
#

COMP_LEVEL=$1		# Compression level
SRC=$2			# Input file


xz --keep -${COMP_LEVEL} ${SRC}
