#!/bin/bash

#zstd -# INPUT_FILE
#
# -#
# 	Copression level, '#' is a digit ch(1 to 19)


COMP_LEVEL=$1		# Compression level
SRC=$2			# Input file


zstd -${COMP_LEVEL} ${SRC}
