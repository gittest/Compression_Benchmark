#!/bin/bash

# gzip --keep -# INPUT_FILE
#
# --keep
# 	Don't delete input file
#
# -#
#	Compression level, '#' is A digit ch(1 to 9)
#

COMP_LEVEL=$1		# Compression level
SRC=$2			# Input file


gzip --keep -${COMP_LEVEL} ${SRC}
