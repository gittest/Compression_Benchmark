#!/bin/bash


### How to RUN ###
# . main.sh 'INPUT_FILE...'


### Compression levles ###
#
# gzip 1 to 9
#
# bzip2 1 to 9
#
# xz 1 to 9
#
# 7z 1 to 9
#
# rar 1 to 5
#
# zstd 1 to 19


### Get test files ###
SRC=$@		# Compressed files(main file)
FILE='file'	# Test file name

### Result file ###
RESULT=result

### Add description of columns and reset result file ###
echo -e 'So&Le\tReal\tUser\tSys\tRatio' > ${RESULT}

# Creat a single file for using in test and add a '.tar' to its end
tar -cf ${FILE}.tar ${SRC} &> /dev/null
FILE=${FILE}.tar
export FILE

### Size fun ###
size() {
	du -b $1 | cut -f1
}
### Ratio fun ###
ratio() {
	echo "scale=3; $(size ${FILE}?*)/$(size ${FILE})*100" | bc
}


source var	# Source Compression level and softwares


for ((d = 0; d < 6; d++))	# A loop for script
do
	for i in $(seq 1 ${LEVEL[ ${d} ]})	# A loop for levels
	do

		# $d is compression software
		# $i is compression level
		{ echo -e "${SCRIPT[ ${d} ]}${i}";
			(time -p (. ${SCRIPT[ ${d} ]}.sh ${i} ${FILE} > /dev/null) 2>&1; ratio ) \
			       | cut -f2 -d' '; } \
			       | tr ' \n' '\t' | tr -s '\t' >> ${RESULT}

			echo >> ${RESULT}

		rm ${FILE}??*	# Delete compressed test file

	done

	echo >> ${RESULT}

done

rm ${FILE}	# Delete file test


