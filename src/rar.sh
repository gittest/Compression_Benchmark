#!/bin/bash

# rar a -m# OUTPUT_FILE INPUT_FILE
#
# a
# 	Add to archives
#
# -m#
# 	Compression level, '#' is a digit ch(1 to 5)
#


COMP_LEVEL=$1		# Comperssion level
SRC=$2			# Input file


rar a -m${COMP_LEVEL} ${SRC}.rar ${SRC}

